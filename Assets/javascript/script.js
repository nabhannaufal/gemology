const section2 = document.getElementById("section2");
const header = document.getElementById("header");
const headerHeight = 90;

function setHeader() {
  const scrollPoint = window.pageYOffset + headerHeight;

  let blockPoint = 90 - (scrollPoint - section2.offsetTop);
  if (blockPoint <= 0) {
    blockPoint = 0;
  }

  if (scrollPoint > section2.offsetTop) {
    header.style.backgroundColor = "#583F21";
  } else {
    header.style.backgroundColor = "#00000080";
  }
  window.requestAnimationFrame(setHeader);
}

function onLinkClick() {
  document.getElementsById("section2").scrollIntoView();
  // will scroll to 4th h3 element
}

window.requestAnimationFrame(setHeader);
